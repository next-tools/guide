First of all: Thanks for helping to test this software.

Second of all: This reader is feature complete for v1.0 so any cool suggestions for new features will be kept in a list 
and inserted into the roadmap for later versions.

Some major features planned but not in the first version are:

    - Showing code (in the Speccy font)
    - Displaying images
    - Bookmarks
    - Mouse support


Setup
=====

The "GUIDE" file is the dot command and should be placed in /dot

The "NextGuide.gde" is the manual and should be placed in the C:/GUIDES folder which you have to create with the
browser or a .mkdir command.

Feel free to create your own guides.  They are simple text files and the instructions of authoring them are in the
manual, which I will allow you to figure out (and thus testing the manual).  The manual itself (NextGuide.gde) has
all the features mentioned within it so use it as an example of usage.


Thanks!
