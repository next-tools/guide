;;----------------------------------------------------------------------------------------------------------------------
;; Global constants used throughout the source code
;;----------------------------------------------------------------------------------------------------------------------

;kScreenHeight           equ     32
;kWindowHeight           equ     (kScreenHeight-2)
;kScreenTop              equ     $4000

kNormalColour           equ     %000'0'111
kStatusColour           equ     %011'1'111
kStatusHiColour         equ     %011'1'100
kErrorColour            equ     %010'1'010
kItalicColour           equ     %000'1'111      ; Bright white
kBoldColour             equ     %000'1'110      ; Yellow
kBoldItalicColour       equ     %000'0'110      ; Bright yellow
kLinkColour             equ     %111'0'000
kSelectColour           equ     %010'1'111      ; Bright white on bright red
kHeader1Colour          equ     %000'0'100      ; Bright green
kHeader2Colour          equ     %000'1'100      ; Green
kHeader3Colour          equ     %000'0'101      ; Bright cyan
kHeader4Colour          equ     %000'1'101      ; Cyan

kLinkCode               equ     $84             ; Start of link text - actually a colour code
kLinkStart              equ     $85             ; End of link text, start of link's link.
kLinkEnd                equ     $86             ; End of link's link.
kLinkSelect             equ     $87

; Address within page stored in NodeLinesPage
kLineOffsetsAddr        equ     $a000
kLinePagesAddr          equ     $b000
kLinkLinesAddr          equ     $b800
kLinkOffsetsAddr        equ     $ba00
kBackBufferAddr         equ     $bb00
kBackBufferPageLimit    equ     $c0             ; Buffer stops before this 256-byte page
kBackBufferPages        equ     5               ; Number of pages in back buffer
