;;----------------------------------------------------------------------------------------------------------------------
;; Console emulation
;;
;; Routines:
;;
;;      ulaMode         switch to ULA mode
;;      textMode        switch to text mode (special use of tilemaps)
;;      at              Move the cursor to a certain position
;;      cursorOn        Turn on the cursor
;;      cursorOff       Turn off the cursor
;;      consoleUpdate   Update cursor
;;      print           Print a message
;;      cls             Clear the screen
;;      input           Fetch up to 79 characters on a row
;;      newline         Go to the next row
;;      setColour       Set colour for text colour slot n
;;      setInk          Set the colour slot for the next text printed

;;----------------------------------------------------------------------------------------------------------------------
;; initConsole
;; Allocates a page for storing $6000-$7fff

ConsolePage     db      0
                db      0

initConsole:
                ; Allocate pages to store MMU2-3
                call    allocPage
                ld      (ConsolePage),a
                nextreg NR_MMU6,a
                call    allocPage
                ld      (ConsolePage+1),a
                nextreg NR_MMU7,a

                ld      hl,$4000
                ld      de,$c000
                ld      bc,$4000
                call    memcpy          ; Back up page 11
                nextreg NR_MMU6,0       ; Restore MMU 6
                nextreg NR_MMU7,1       ; Restore MMU 7

                ; Load font
                dos     M_GETHANDLE
                ld      hl,$6000        ; HL = start of tilemap data
                ld      bc,FontSize
                dos     F_READ

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; doneConsole
;; Restore page 11

doneConsole:
                call    ulaMode
                ld      a,(ConsolePage)
                nextreg NR_MMU6,a
                ld      a,(ConsolePage+1)
                nextreg NR_MMU7,a
                ld      hl,$c000
                ld      de,$4000
                ld      bc,$4000
                call    memcpy
                ld      a,(ConsolePage)
                call    freePage
                ld      a,(ConsolePage+1)
                call    freePage

                ret


;;----------------------------------------------------------------------------------------------------------------------
;; ulaMode

ulaMode:
                xor     a
                nextreg $68,a           ; Enable ULA
                nextreg $6b,0           ; Disable tilemap

                nextreg NR_EULA_CTRL,%0'000'000'0
                nextreg NR_PAL_IDX,$10
                xor     a
                nextreg NR_EULA_PAL,a
                nextreg NR_EULA_PAL,a

                ld      a,($5c48)       ; Get border colour
                rrca
                rrca
                rrca                    ; Get the colour field
                out     (IO_ULA),a

                ret

;;----------------------------------------------------------------------------------------------------------------------

textMode:
                ; Set up the border
                xor     a
                out     (IO_ULA),a
                nextreg NR_EULA_CTRL,0
                nextreg NR_PAL_IDX,$10
                ld      a,(Palette)
                nextreg NR_EULA_PAL,a
                ld      a,(Palette+1)
                nextreg NR_EULA_PAL,a

                nextreg NR_TMAP_CTRL,%11001001          ; Tilemap control
                nextreg NR_TMAP_BASE,$00                ; Tilemap base offset                           $4000-$4f00 (80*24*2)
                nextreg NR_TILES_BASE,$20               ; Tiles base offset                             $6000-$7000 (8*512)
                nextreg NR_TMAP_TRANS,8                 ; Transparency colour (bright black)
                ;nextreg NR_ULA_CTRL,%10000000           ; Disable ULA output

                ; Reset scrolling and clip window
                xor     a
                nextreg NR_CLIP_TILEMAP,a
                nextreg NR_CLIP_TILEMAP,159
                nextreg NR_CLIP_TILEMAP,a
                nextreg NR_CLIP_TILEMAP,255             ; Reset clip window
                nextreg NR_TMAP_XMSB,a
                nextreg NR_TMAP_XLSB,a
                nextreg NR_TMAP_Y,a                     ; Reset scrolling
                call    cls

                ; Initialise the tilemap palette
                nextreg NR_EULA_CTRL,%00110000          ; Set tilemap palette
                xor     a
                nextreg NR_PAL_IDX,a                    ; Start from index 0
                ld      b,a                             ; Colour (PPPIIII)
                ld      de,Palette

.l1             
                ; Set the paper colour
                ld      a,b
                and     $70
                swapnib                 ; a = 00000PPP
                ld      hl,de
                add     a,a
                add     hl,a
                ld      a,(hl)
                nextreg NR_EULA_PAL,a
                inc     hl
                ld      a,(hl)
                nextreg NR_EULA_PAL,a

                ; Set the ink colour
                ld      a,b
                and     $f
                ld      hl,de
                add     a,a
                add     hl,a            ; HL = address of palette colour
                ld      a,(hl)
                nextreg NR_EULA_PAL,a
                inc     hl
                ld      a,(hl)
                nextreg NR_EULA_PAL,a

                inc     b
                jp      p,.l1

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Palette control

Palette:                            ;       R       G       B
                db  %01001001, %0   ;       3       3       3       Dark grey
                db  %00000011, %1   ;       0       0       7       Bright blue
                db  %11100000, %0   ;       7       0       0       Bright red
                db  %00000000, %0   ;       0       0       0       Black           Black is here so we can have a black bg
                db  %00011100, %0   ;       0       7       0       Bright green
                db  %00011111, %1   ;       0       7       7       Bright cyan
                db  %11111100, %0   ;       7       7       0       Bright yellow
                db  %10110110, %1   ;       5       5       5       Light grey

                db  %10100010, %1   ;       5       0       5       Magenta         We'll never have a magenta background
                db  %00000010, %1   ;       0       0       5       Blue
                db  %10100000, %0   ;       5       0       0       Red
                db  %11100011, %1   ;       7       0       7       Bright magenta
                db  %00010100, %0   ;       0       5       0       Green
                db  %00010110, %1   ;       0       5       5       Cyan
                db  %10110100, %0   ;       5       5       0       Yellow
                db  %11111111, %1   ;       7       7       7       White

;;----------------------------------------------------------------------------------------------------------------------
;; cls
;; Clears the screen

cls:
        ; todo - set char 0 as blank to and use DMA to fill the tilemap
        ; Clear screen (write spaces in colour 0 everywhere)
        ; Uses:
        ;       BC, HL, A
        ;
                ld      hl,$4000
                ld      bc,5120
                xor     a
                call    memfill
                ld      bc,0
                call    at
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; at
;; Sets the cursor position

CurrentCoords   dw      0       ; Byte 0: X, Byte 1: Y
CurrentPos:     dw      0

at:
        ; Input:
        ;       BC = YX coord
        ;
        ; Output:
        ;       HL = tile address
        ;       CurrentPos set to tile address too
        ;       A = X
        ;
                ld      (CurrentCoords),bc
                push    de
                ld      e,b
                ld      d,80
                mul                     ; DE = 80Y
                ex      de,hl
                ld      a,c
                add     hl,a            ; HL = 80Y + X
                add     hl,hl           ;    = (80Y + X) * 2
                ld      de,$4000
                add     hl,de           ; Current position
                ld      (CurrentPos),hl
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; setColour
;; Set the current colour for printing
;;
;; Input:
;;              A = colour (0 PPP B III)        P = Paper, B = Bright, I = Ink
;; Output:
;;              A = tilemap attribute for text mode

setColour:
                add     a,a
                ld      (CurrentColour),a
                ret

CurrentColour   dw      %00000111

;;----------------------------------------------------------------------------------------------------------------------
;; print
;; Print the text at DE
;;
;; Supports these codes:
;;
;;      %BPI    B(right) = 0/1, P(aper)=0..7, I(nk)=0..7
;;      %%      '%' character
;;      %n      Normal colour (%007)
;;      %b      Bright colour (%107)


SpaceString     db      "  ",0

print:
        ; todo - speed this up (perhaps not updating coords every character)
        ; Input:
        ;       DE = text
        ;
                push    bc
                push    hl
                ld      hl,(CurrentPos)
                ld      a,(CurrentColour)
                ld      c,a             ; C = tilemap attribute

.l1             ld      a,(de)          ; Fetch character
                and     a
                inc     de              ; Next character pointer
                jr      z,.finish       ; End of string?
                jr      .not_cr         ; Carriage return

                ; Go to next line
.nextline       inc     b
                ld      c,0
                call    at
                jr      .next_char

.not_cr
                ld      (hl),a          ; Write out character
                inc     hl
                ld      (hl),c          ; Write out attribute
                inc     hl

                ; Update coords
                push    bc
                ld      bc,(CurrentCoords)
                inc     c
                ld      a,c
                cp      80
                jr      z,.nextline

                ; Go to next line
.next_char      ld      (CurrentCoords),bc
                pop     bc
                jr      .l1
                
.finish:
                ld      (CurrentPos),hl
                pop     hl
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; printChar

CharBuffer:     db      0,0

printChar:
        ; Input:
        ;       A = character
        ;
                push    de
                ld      (CharBuffer),a
                ld      de,CharBuffer
                call    print
                pop     de
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; Drawing routines

calcTileAddress:
        ; Input:
        ;   B = Y coord (0-31)
        ;   C = X coord (0-79)
        ; Output:
        ;   HL = Tile address
        ; Destroys:
        ;   BC
        push    de
        ld      e,b
        ld      d,80
        mul                 ; DE = 80Y
        ex      de,hl       ; HL = 80Y
        pop     de
        ld      b,0
        add     hl,bc       ; HL = 80Y+X
        add     hl,hl       ; 2 bytes per tilemap cell
        ld      bc,$4000    ; Base address of tilemap
        add     hl,bc
        ret

;;----------------------------------------------------------------------------------------------------------------------
;; writeSpace
;; Draw a rectangle of space
;;
;; Input:
;;      B = Y coord (0-31) of start
;;      C = X coord (0-79) of start
;;      D = height
;;      E = width
;;      A = colour

writeSpace:
        push    af
        push    bc
        push    de
        push    hl
        call    calcTileAddress     ; HL = start corner
        add     a,a
        ld      c,a                 ; C = colour
        ld      b,e                 ; Save width
.row    ld      e,b                 ; Restore width

        push    hl

.col    ld      a,' '               ; Write space
        ld      (hl),a
        inc     hl
        ld      (hl),c              ; Write colour
        inc     hl
        dec     e
        jr      nz,.col

        ; Move HL to next row
        pop     hl
        ld      e,160
        ld      a,d                 ; Save row counter
        ld      d,0
        add     hl,de               ; HL = next row
        ld      d,a
        dec     d
        jr      nz,.row
        pop     hl
        pop     de
        pop     bc
        pop     af
        ret

;;----------------------------------------------------------------------------------------------------------------------
;; writeText
;; Convenience routine to write text at a position in a certain colour
;;
;; Input:
;;      BC = YX coord
;;      DE = Text
;;      A = Colour (0PPPIIII)

writeText:
        call    setColour
        call    at
        call    print
        ret

;;----------------------------------------------------------------------------------------------------------------------
;; printNum
;; Write an unsigned numeric value in decimal
;;
;; Input:
;;      DE = Number

printNum:
        push    af
        push    bc
        push    de
        push    hl

        ld      h,d
        ld      l,e
        ld      de,.text
        push    de
        call    .convert
        xor     a
        ld      (de),a          ; Null terminate string

        pop     de              ; Restore text pointer

.next_digit:
        ld      a,(de)
        cp      '0'
        jr      nz,.print       ; Number ready to print
        inc     de
        jr      .next_digit

.print  call    print

        pop     hl
        pop     de
        pop     bc
        pop     af
        ret

        
.convert
        ld      bc,-10000
        call    .conv
        ld      bc,-1000
        call    .conv
        ld      bc,-100
        call    .conv
        ld      c,-10
        call    .conv
        ld      c,-1
.conv   ld      a,'0'-1
.l1     inc     a
        add     hl,bc
        jr      c,.l1
        sbc     hl,bc
        ld      (de),a
        inc     de
        ret
        
.text   ds      6
