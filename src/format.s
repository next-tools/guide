;;----------------------------------------------------------------------------------------------------------------------
;; Scans the source text file and generates a document and meta-data.
;;
;;----------------------------------------------------------------------------------------------------------------------

; Scan state
; 0 = Finished reading
; 2 = detecting line type
; 4 = reading paragraph
; 6 = parsing command

kCmdFinish      equ     0
kCmdDetect      equ     2
kCmdLine        equ     4
kCmdParseCmd    equ     6

; Node data addresses
NodeInfo        equ     $a000
NodeNames       equ     $a800

ErrorState      db      0               ; If not 0, don't write any more data

kErrorMaxNodes  equ     1               ; Maximum number of nodes reached.
kErrorBadName   equ     2
kErrorDuplicate equ     3               ; Node has duplicate name

StateTable:
                dw      finishedScan
                dw      categoriseLine
                dw      readLine        ; Normal text - add it to current node
                dw      parseCmd        ; @command - figure out the command and handle it

BufferPtrPage:  db      0
BufferPtr:      dw      0               ; Pointer to read from

NumNodes        db      0               ; Number of nodes seen so far
NumNames        db      0               ; Number of names seen so far
CurrentNode     db      $ff             ; Current node index we're writing to.
CurrentLineLen  db      0               ; Current line length after it was fetched.

;;----------------------------------------------------------------------------------------------------------------------
;; startDoc
;; Start creating a document.  It allocates the pages and arenas that is required and initialises some state.

startDoc:
                ; Allocate a page to hold the node meta data and page it into MMU5
                call    allocPage
                and     a
                call    z,oom
                ld      (NodesPage),a
                nextreg $55,a                   ; $a000-bfff will contain node entries

                ; Initialise the arena to contain the formatted document
                call    arenaNew
                call    z,oom
                ld      (DocArena),a

                ; Initialise state
                ld      a,$ff
                ld      (IndexNode),a
                inc     a
                ld      (NumNodes),a
                ld      (NumNames),a
                ld      (ErrorState),a
                inc     a

                ret


;;----------------------------------------------------------------------------------------------------------------------
;; checkError
;; CF = 1 if an error had occured

checkError:
                push    af
                ld      a,(ErrorState)
                and     a
                jr      nz,.error
                ld      a,(FatalError)
                and     a
                jr      nz,.error
                pop     af
                and     a
                ret
.error:
                pop     af
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; formatDoc
;; Scan the buffer and produce a formatted document that the view routine can render
;;
;; Output:

formatDoc:
                call    startDoc
                call    checkError
                ret     c
                ld      (BufferPtrPage),a       ; Page #1 in the buffer arena (page #0 is the meta data)
                inc     a                       ; A = 2 (current scan state = categorise line)
                ld      hl,0
                ld      (BufferPtr),hl

                ; Run the state machine to process the lines

.next_line:
                ; HL = current line start

                ; Call the routine of the current scan state.  The routine is expected to return the next state
                ; in A.
                ld      hl,StateTable
                add     hl,a
                ldhl
                callhl
                call    checkError
                jr      nc,.next_line
                ret                             ; Exit early on error

finishedScan:   pop     af                      ; Drop the return address

                ; Append a final terminator
                call    endGenerationNode       ; Terminate the last node made

                ; Generate the About node
                call    generateAboutNode

                ; Post-process the node links
                ld      a,(NumNodes)
                ld      b,a                     ; B = number of nodes to process
                ld      c,0

.l1             push    bc
                call    getNodeInfoAddr         ; HL = data for node
                inc     hl
                inc     hl
                inc     hl                      ; HL = next node
                call    adjustIndex
                inc     hl                      ; HL = previous node
                call    adjustIndex
                inc     hl                      ; HL = Table of contents node
                call    adjustIndex
                pop     bc
                inc     c
                djnz    .l1

                ; Adjust index for the index
                ld      hl,IndexNode
                call    adjustIndex

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; adjustIndex
;; Given an index into the node name map, convert to the real node index

adjustIndex:
                ld      a,(hl)                  ; Get node index
                cp      $ff                     ; Is it valid?
                ret     z                       ; No, no conversion required
                ld      b,a
                push    hl
                call    getNodeNamesAddr        ; HL = data for node name
                ld      a,(hl)                  ; Get actual index
                pop     hl
                ld      (hl),a                  ; Update index
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; categoriseLine
;; Scan the line to discover what it is
;;
;; It's either:
;;
;;      1) A normal line
;;      2) A guide command (starts with '@')
;;

categoriseLine:
                call    fetchLine               ; Fetch a single line at DE
                ld      a,l
                ld      (CurrentLineLen),a
                jr      c,.finished
                ld      a,(de)
                cp      '@'                     ; Command
                jr      z,.commandLine

                ; This is a normal line (that may contain links)
.line           ld      a,kCmdLine
                ret

.commandLine:
                inc     de
                ld      a,(de)
                dec     de
                cp      '{'                     ; Is it a normal embedded command?
                jr      z,.line                 ; Yes, it's not a command
                ld      a,kCmdParseCmd
                ret

.finished       ld      a,kCmdFinish
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fetchLine
;; Read a whole line from the buffer and place it in the CurrentLine buffer.  Lines longer than 255 characters will get
;; terminated.  CurrentLine is nul-terminated.
;;
;; If there are no more lines, the CF is set.
;;
;; Output:
;;      DE = address of CurrentLine
;;      L = length of line
;;      CF = 1 if no more lines, 0 if CurrentLine is filled.
;;

fetchLine:
                ; Bring current buffer position into MMU6
                ld      a,(BufferPtrPage)
                ld      e,a
                ld      a,(BufferPage)
                ld      hl,(BufferPtr)
                call    arenaPrepare            ; HL = real address between $c000-$dfff
                ld      de,CurrentLine          ; E guaranteed to be 0
                ld      ix,(BufferMeta)

                call    isAtEnd
                jr      nc,.l1

                scf
                ret

.l1             
                ; Check to see if we have reached the end of the buffer
                call    isAtEnd
                jr      nc,.cont

                ; Reached the end of the buffer
.eol
                xor     a
                ld      (de),a                  ; nul terminate it
                ld      hl,CurrentLine
                ex      de,hl
                sbc     hl,de                   ; HL = line length
                ld      de,CurrentLine

                ret

.cont
                call    nextBufferChar          ; Grab next character in A
                cp      $0a                     ; End of line?
                jr      z,.eol
                cp      $0d                     ; Possible end?
                jr      nz,.not_eol

                ; Check for $0d$0a sequence
                call    isAtEnd
                jr      c,.eol
                ld      a,(hl)
                cp      $0a
                jr      nz,.eol
                call    nextBufferChar          ; Consume the $0a
                jr      .eol

.not_eol
                ld      (de),a
                inc     e
                jr      nc,.l1

                ; Reached end of fetch buffer - keep fetching characters until we get a newline
.l2             call    nextBufferChar
                and     a
                jr      z,.term
                cp      $0a
                jr      z,.term
                cp      $0d
                jr      nz,.l2

.term
                dec     e
                jr      .eol

nextBufferChar:
                ; HL = address of current char
                ld      a,(hl)
                push    af
                inc     hl
                ld      a,h
                cp      $e0                     ; Past 8K?
                jr      nz,.writeHL

                ; Go to next page
                push    de
                ld      a,(BufferPtrPage)
                inc     a
                ld      (BufferPtrPage),a
                ld      e,a
                ld      a,(BufferPage)
                ld      hl,0
                call    arenaPrepare
                pop     de
.writeHL
                ld      a,h
                and     $1f
                ld      h,a
                ld      (BufferPtr),hl
                or      $c0
                ld      h,a
                pop     af
                ret

isAtEnd:
                exx
                ld      a,(BufferPtrPage)
                cp      (ix+Buffer.Size)        ; In the last 8K chunk?
                jr      nz,.cont                ; No, continue - we're not at the end
                ld      hl,(BufferPtr)
                ld      de,(ix+Buffer.LastSize)
                call    compare16
                jr      nz,.cont
                scf
                exx
                ret

.cont
                exx
                and     a
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; readLine
;; Read in a line from the buffer and add it to the document.

readLine:
                ; Current state:
                ;       CurrentLine contains current line of text
                ;       CurrentNode is the current node we're adding text to (or $ff if not in a node)

                ; Check to see if we're before any nodes
                ld      a,(CurrentNode)
                cp      $ff
                jp      z,.end          ; All text outside of a node is ignored

                ld      a,(CurrentLineLen)
                ld      b,a
                call    parseLine       ; Parse the text in CurrentLine and put the result in Scratch

                ; Store text in format arena
                ld      de,Scratch
                ld      bc,hl
                ld      a,(DocArena)
                call    arenaPushData
                call    c,oom

                ld      a,(DocArena)
                ld      c,$0a           ; Append buffer with newline
                call    arenaPushByte
                call    c,oom

.end            ld      a,kCmdDetect
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; parseCmd

CommandTable:
                db      4,"NODE"
                db      5,"INDEX"
                db      3,"REM"
                db      3,"TOC"
                db      4,"NEXT"
                db      4,"PREV"
                db      5,"TITLE"
                db      6,"AUTHOR"
                db      7,"VERSION"
                db      4,"DATE"
                db      5,"BUILD"
                db      9,"COPYRIGHT"
                db      0

CommandHandlers:
                dw      cmdNode
                dw      cmdIndex
                dw      cmdRemark
                dw      cmdToc
                dw      cmdNext
                dw      cmdPrev
                dw      cmdTitle
                dw      cmdAuthor
                dw      cmdVersion
                dw      cmdDate
                dw      cmdBuild
                dw      cmdCopyright

parseCmd:
                ; Figure out the start and end of the next word and convert to command index
                inc     de              ; Skip past the '@'
                ld      h,Scratch/256
                call    fetchWord       ; Move next word into scratch
                call    scratchStrUpper ; Ensure scratch string is uppercase
                push    de
                ld      de,CommandTable
                call    matchWord       ; Compare scratch to all commands to get index
                pop     de
                jr      nc,.invalid_command

                ld      hl,CommandHandlers
                add     a,a
                add     hl,a
                ldhl
                callhl

                call    checkError
                jr      nc,.no_error
                ld      a,kCmdFinish
                ret
.no_error       ld      a,kCmdDetect
                ret

.invalid_command:
                ld      a,kCmdDetect
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Command handlers


checkName:
                ; Check that the contents of Scratch is not in the names list
                ; If found, set CF and B = node name index
                ; If not found, CF = 0, B is unchanged
                ;
                ; Set HL and call checkNameHL to use different buffer
                ld      hl,Scratch

checkNameHL:
                ld      a,(NumNames)
                and     a
                ret     z               ; No names added so not found (also CF = 0)
                push    bc
                push    hl
                ld      b,a             ; B = num names
                ld      c,0             ; C = current index

                ld      de,NodeNames+1
.l1             pop     hl
                push    hl
                call    strcmp
                jr      z,.found

                ld      a,16
                add     de,a            ; DE = next entry
                inc     c
                djnz    .l1

                pop     hl
                and     a
                pop     bc
                ret

.found          pop     hl
                ld      a,c
                scf
                pop     bc
                ld      b,a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; getNodeInfoAddr
;; Given a node index in C, return the address in HL

getNodeInfoAddr:
                push    de
                ld      e,c
                ld      d,0             ; DE = number of nodes
                ex      de,hl
                add     hl,hl
                add     hl,hl
                add     hl,hl
                add     hl,NodeInfo     ; HL = node info address
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; getNodeInfoAddrX
;; Given a node index in A, return the address in IX
;;
;; Destroys: HL
;;

getNodeInfoAddrX:
                ld      l,a
                ld      h,0
                add     hl,hl
                add     hl,hl
                add     hl,hl
                add     hl,NodeInfo
                push    hl
                pop     ix
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; getNodeNamesAddr
;; Given a node index in B, return the address in HL

getNodeNamesAddr:
                push    bc
                push    de
                ld      hl,NodeNames
                ld      e,b
                ld      d,0
                ld      b,4
                bsla    de,b
                add     hl,de           ; HL = address of node name
                pop     de
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; getNodeName
;; Reads a node name from the current line (truncates it to 15 characters) and attempts to find it in the node name
;; table or create a new one.
;;
;; Output:
;;      B = name index
;;      C = node index
;;      CF = 1: error occurred
;;

getNodeName:
                ld      h,Scratch/256
                call    fetchQuotedWord ; Grab node name
                ld      a,(Scratch)
                and     a
                jr      nz,.not_empty
                ld      a,kErrorBadName
                ld      (ErrorState),a
                ret
.not_empty
                ld      hl,Scratch+15
                xor     a
                ld      (hl),a          ; Terminate to 15 characters
                call    scratchStrUpper ; Name are case insensitive (H already points to buffer)

                ; Set up BC
                ; B = node name index
                ; C = node index
                ld      a,(NumNames)
                ld      b,a             ; Assuming that name has not been seen before
                ld      a,(NumNodes)
                ld      c,a             ; C = new node index

                ; Test to see if we haven't seen it before
                call    checkName
                jr      c,.end

                ; Check to see if we have enough space
                ld      a,b
                cp      $ff
                jr      z,.out_mem
                ld      a,c
                cp      $ff
                jr      nz,.enough_mem

.out_mem:
                ld      a,kErrorMaxNodes
                ld      (ErrorState),a
                scf
                ret

.enough_mem:
                ; Add the new node to the node names page
                call    getNodeNamesAddr
                ld      a,$ff
                ld      (hl),a          ; Store unknown index
                inc     hl
                ex      de,hl
                ld      hl,Scratch
                push    bc
                ld      bc,14
                ldir
                pop     bc
                xor     a
                ld      (de),a          ; Write name and null terminate
                ld      a,(NumNames)
                inc     a
                ld      (NumNames),a

.end:           and     a
                ret
                

;;----------------------------------------------------------------------------------------------------------------------
;; cmdNode
;; Handles the @node command
;;
;; Syntax: @node <name> (15 chars max)

cmdNode:
                call    getNodeName             ; B = Node name
                ret     c

                ; Mark end of previous Node
                ld      a,(DocArena)
                ld      c,0
                call    arenaPushByte
                call    c,oom
                ret     c

                ; Add the new node info into the node info page
                ld      a,(NumNodes)
                ld      c,a
                inc     a
                ld      (NumNodes),a

                ; Fill in the node name entry with the real index, which should already be $ff
                call    getNodeNamesAddr
                ld      a,(hl)
                cp      $ff
                jr      nz,.duplicate
                ld      (hl),c

                ; Create a blank node info entry
                ld      a,(DocArena)
                call    arenaGetAddr    ; A = page index, HL = page offset
                ex      de,hl           ; DE = page offset
                call    getNodeInfoAddr
                ldi     (hl),a          ; Write page index
                ldi     (hl),de         ; Write page offset

                ; Fill in the rest with invalid links (next, prev, toc etc.)
                ld      a,$ff
                ldi     (hl),a                  ; Unknown next node
                ldi     (hl),a                  ; Unknown prev node
                ldi     (hl),a                  ; Unknown TOC node

                ; Link to new name
                ld      (hl),b                  ; Name index

                ; Set up the current node state
                ld      a,c
                ld      (CurrentNode),a
                ret

.duplicate      ld      a,kErrorDuplicate
                ld      (ErrorState),a
                scf
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; generateNewNode
;; Generate the beginning of a new node.  Write codes to DocArena afterwards and then terminate with $00.
;;
;; Input:
;;      HL = name (ensure it is null terminated and maximum 15 characters)
;;
;; Output:
;;      A = node#
;;

generateNewNode:
                push    hl

                ; Allocate a new node
                ld      a,(NumNodes)
                ld      (AboutNode),a
                ld      c,a
                inc     a
                ld      (NumNodes),a

                ; Insert a new name: [ABOUT]
                ld      a,(NumNames)
                ld      b,a
                inc     a
                ld      (NumNames),a

                ; Set up the node name
                call    getNodeNamesAddr
                ld      a,(NumNodes)
                ldi     (hl),c
                ex      de,hl
                pop     hl

.l1             ldi     a,(hl)
                ldi     (de),a
                and     a
                jr      nz,.l1

                ; Set up the node
                ld      a,(DocArena)
                call    arenaGetAddr
                ex      de,hl                   ; DE = page offset
                call    getNodeInfoAddr         ; HL = node info address
                ldi     (hl),a                  ; Write page index
                ldi     (hl),de                 ; Write page offset
                ld      a,$ff
                ldi     (hl),a                  ; No next link
                ldi     (hl),a                  ; No prev link
                ldi     (hl),a                  ; No toc link
                ld      (hl),b                  ; Insert name link

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; endGenerationNode
;; End the generation of the new node

endGenerationNode:
                ld      a,(DocArena)
                ld      c,0
                call    arenaPushByte
                call    c,oom

                ret


;;----------------------------------------------------------------------------------------------------------------------
;; generateAboutNode
;; Generates a new node with the internal name 

generateAboutNode:
                ld      hl,AboutName
                call    generateNewNode

                ; Start writing out the document
                call    writeDoc
                db      $0a,$88,"                              ABOUT THIS DOCUMENT",$80,$0a,$0a
                db      0

                ld      hl,MetaTitle
                call    writeMeta
                dz      '    Title: '
                ld      hl,MetaAuthor
                call    writeMeta
                dz      '   Author: '
                ld      hl,MetaVersion
                call    writeMeta
                dz      '  Version: '
                ld      hl,MetaDate
                call    writeMeta
                dz      '     Date: '
                ld      hl,MetaBuild
                call    writeMeta
                dz      '    Build: '
                ld      hl,MetaCopyright
                call    writeMeta
                dz      'Copyright: '

                call    writeDoc
                db      $0a,$0a,$0a,$88,"                           ABOUT THE NEXTGUIDE CLIENT",$80,$0a,$0a
                db      $89,'  Version: ',$80
                GUIDE_VERSION
                db      0

                jp      endGenerationNode


AboutName:      dz      '[ABOUT]'

;;----------------------------------------------------------------------------------------------------------------------
;; generateErrorNode
;; Generates a new node with an error message

generateErrorNode:
                ld      hl,ErrorName
                call    generateNewNode

                call    writeDoc
                db      $0a,$88,"ERROR OCCURRED",$0a
                dz      "==============",$80,$0a,$0a

                ld      hl,ErrorTable
                ld      a,(ErrorCode)
                add     hl,a
                ldhl
                call    writeDocHL

                jp      endGenerationNode

ErrorName:      dz      '[ERROR]'

;;----------------------------------------------------------------------------------------------------------------------
;; writeDoc
;; Read the data after the call and insert it into the document
;;

writeDoc:
                ex      (sp),hl                 ; HL = data
                call    writeDocHL
                ex      (sp),hl
                ret

writeDocHL:
.l1:            ldi     a,(hl)
                and     a
                jr      z,.done

                ld      c,a
                ld      a,(DocArena)
                push    hl
                call    arenaPushByte
                pop     hl
                call    c,oom
                jr      .l1

.done:
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; writeMeta
;; Write out meta information into an About node
;;
;; Input:
;;      HL = buffer to display
;;

writeMeta:
                ; If the buffer string is empty, then do not display it.
                ld      a,(hl)
                or      a
                jr      nz,.write

                ; Find the end of the data that follows call
                ex      (sp),hl
                call    endString
                inc     hl
                ex      (sp),hl
                ret

.write:
                ; Write out the prefix that follows the call
                call    writeDoc
                dz      $89                     ; Header 2 format

                ex      (sp),hl                 ; HL = prefix text
                call    writeDocHL
                ex      (sp),hl                 ; Restore HL

                call    writeDoc
                dz      $80                     ; Normal format

                ; Write out the buffer
                call    writeDocHL

                ; End the line
                call    writeDoc
                dz      $0a

                ret
                
;;----------------------------------------------------------------------------------------------------------------------

cmdRemark:
                ; This command does nothing
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdNext
;; Handles the @next command
;;

cmdNext:
                call    getNodeName             ; B = node name
                ret     c
                ld      a,(CurrentNode)
                ld      c,a
                call    getNodeInfoAddr         ; HL = node info
                ld      a,3
                add     hl,a
                ld      (hl),b
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdPrev
;; Handles the @prev command
;;

cmdPrev:
                call    getNodeName             ; B = node name
                ret     c
                ld      a,(CurrentNode)
                ld      c,a
                call    getNodeInfoAddr         ; HL = node info
                ld      a,4
                add     hl,a
                ld      (hl),b
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdToc
;; Handles the @toc command
;;

cmdToc:
                call    getNodeName             ; B = node name
                ret     c
                ld      a,(CurrentNode)
                ld      c,a
                call    getNodeInfoAddr         ; HL = node info
                ld      a,5
                add     hl,a
                ld      (hl),b
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdIndex
;; Handles the @index command

cmdIndex:
                call    getNodeName             ; B = node name
                ret     c                       ; Return if an error occurred
                ld      a,b
                ld      (IndexNode),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; skipWS
;; Advance HL until we get past the whitespace in the text buffer pointed to by HL.

skipWS:
                ld      a,(hl)
                cp      ' '
                jr      z,.skip
                cp      9
                jr      z,.skip
                ret
.skip           inc     hl
                jr      skipWS

;;----------------------------------------------------------------------------------------------------------------------
;; parseLine

fontMode        db      0               ; 1 = italic

parseLine:
                ; CurrentLine = source text
                ; Scratch = destination
                ; B = length of line
                ld      hl,CurrentLine
                ld      de,Scratch

.l1             ld      a,(hl)
                and     a               ; Reached end?
                jr      z,.end
                inc     hl
                cp      '@'             ; Special code?
                jr      nz,.next

                ; We've found a possible command, but we expect a '{' to follow
                ; If not, just add a @
                ldi     a,(hl)
                cp      '{'
                jp      z,.command
                cp      '@'
                jp      z,.at
                cp      '('
                jp      z,.copyright
                dec     hl
.at             ld      a,'@'
                jp      .next
.copyright      ld      a,127
                jp      .next


.command        ; We have a command.
                ldi     a,(hl)
                and     a               ; Reached end?
                jr      z,.end
                cp      ' '
                jr      z,.command      ; Skip spaces
                cp      9
                jr      z,.command      ; Skip tabs
                cp      '}'
                jr      z,.l1           ; End of command - get next character
                cp      'i'
                jp      z,.italic       ; Handle "italics"
                cp      'b'
                jp      z,.bold         ; Handle "bold"
                cp      'u'
                jp      z,.undo         ; Handle turning off features
                cp      $22
                jp      z,.link         ; Links
                cp      'c'
                jp      z,.centre       ; Centre whole line
                cp      'r'
                jp      z,.right        ; Right justify line
                cp      'h'
                jp      z,.header       ; Headers

                ; Unknown command - ignore and find end of command or line
.find_end
                ldi     a,(hl)
                cp      '}'
                jr      z,.l1
                and     a
                jr      z,.end
                jr      .find_end

.next           ldi      (de),a
                jr      .l1

.end:
                ld      a,(fontMode)
                bit     3,a             ; Did we use a header flag?
                jr      z,.no_header
                xor     a
                ld      (fontMode),a    ; Yes, reset to normal font
                ld      a,$80           ; And reset the colour coding
                ldi     (de),a
.no_header:
                xor     a
                ld      (de),a          ; Null terminate the string
                ld      hl,Scratch
                ex      de,hl
                sbc     hl,de           ; HL = length

                ld      a,(.CentreFlag)
                and     a
                ret     z               ; No centring required

                ; TODO: Calculate real length for number of spaces calculation
                ; Work out how many spaces to insert
                ld      de,80
                call    compare16
                ret     nc              ; Return is size is greater than 80 characters

                push    hl              ; Store length of text
                ld      de,hl           ; DE = length

                ; Handle right-aligned set up parameters
                ld      hl,80           ; Length to calculate against for right-aligned

                cp      2
                jr      z,.do_calc:

.do_centre:
                ; Centre-aligned set up parameters
                inc     de
                srl     d
                rr      e               ; DE = length/2
                ld      hl,40

.do_calc:
                and     a
                sbc     hl,de           ; HL = number of spaces required

                xor     a
                ld      (.CentreFlag),a         ; Reset it for next line

                ; HL = number of spaces
                ; Need to set up a memory move:
                ;       HL = Scratch
                ;       DE = Scratch+number of spaces
                ;       BC = length of text
                ld      de,Scratch
                add     hl,de
                ld      de,Scratch
                ex      de,hl           ; HL = source, DE = destination
                pop     bc              ; BC = number of bytes to copy
                push    bc
                inc     bc              ; Include the null terminal
                call    memcpy_r        ; Reverse copy

                ; Now we need to clear the initial area with spaces
                ex      de,hl
                and     a
                sbc     hl,de           ; HL = number of spaces
                ld      bc,hl           ; BC = number of spaces
                ld      a,' '
                ld      hl,Scratch
                call    memfill         ; Fill it!

                pop     hl              ; Restore the old length
                add     hl,bc           ; Add the number of spaces added
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Formatting

.CentreFlag     db      0

.centre:
                ld      a,1
                ld      (.CentreFlag),a
                jr      .find_end

.right:
                ld      a,2
                ld      (.CentreFlag),a
                jr      .find_end

;;----------------------------------------------------------------------------------------------------------------------
;; Links

.link:
                ld      a,kLinkCode
                ld      (de),a          ; Write it
                inc     e

                ; Continue writing the text within the quotes
.l2             ld      a,(hl)
                inc     l
                and     a
                jr      z,.link_end     ; Found the end?
                cp      $22
                jr      z,.link_end     ; Matching quotes?

                ld      (de),a
                inc     e
                jr      .l2

.link_end:
                ; Read the action word
                ; HL = source (CurrentLine)
                ; DE = destination (Scratch)
                push    bc
                call    skipWS
                ld      a,(hl)
                and     a               ; End of source?
                jr      z,.no_action

                push    de
                ex      de,hl           ; DE = Source
                ld      h,Scratch2/256
                call    fetchWord       ; Scratch2 contains the action word
                call    scratchStrUpper
                push    de              ; Push source
                ld      de,ActionTable
                call    matchWord
                pop     hl              ; Restore source
                pop     de              ; DE = destination (within Scratch)
                jr      nc,.no_action

                ; A = index of action word
                ; HL = source
                ; DE = destination
                ld      (CurrentAction),a
                ld      c,a             ; C = index
                push    de              ; Store destination
                ex      de,hl           ; DE = source
                ld      h,Scratch2/256  ; H = buffer to read into
                call    fetchQuotedWord
                ld      a,c             ; A = action index
                ld      bc,de           ; BC = source
                ld      l,0             ; HL = argument
                pop     de              ; DE = destination
                push    bc              ; Store source
                add     a,a
                ld      bc,ActionHandlers
                add     bc,a            ; BC = address of handler
                push    .finishLink     ; Push return address for handler
                ld      a,(bc)          ; A = LSB of handler address
                exa
                inc     bc
                ld      a,(bc)          ; A = MSB of handler address
                ld      b,a
                exa
                ld      c,a     
                push    bc
                ret                     ; JP (BC)
.no_action

                ; Add empty link data
                ld      a,kLinkStart
                ld      (de),a
                inc     e
                ld      a,kLinkEnd
                ld      (de),a
                inc     e
                jr      .finishLink2

                ; Terminate link with return to previous font mode
.finishLink
                pop     hl              ; Restore source
.finishLink2
                pop     bc              ; Restore old BC
                ld      a,(fontMode)
                or      $80
                ldi     (de),a
                jp      .find_end


;;----------------------------------------------------------------------------------------------------------------------
;; Colour codes (bold, italic etc)

.header:
                ; Grab next character
                ld      a,(hl)
                and     a
                ret     z               ; End of line?
                ld      b,8
                cp      '1'
                jr      z,.apply_mode
                inc     b
                cp      '2'
                jr      z,.apply_mode
                inc     b
                cp      '3'
                jr      z,.apply_mode
                inc     b
                cp      '4'
                jr      z,.apply_mode

                ; Unknown header character
                jp      .find_end


.bold:
                ; Switch on "bold mode"
                ld      b,2
                jr      .apply_mode

.italic:
                ; Switch on "italic mode"
                ld      b,1

.apply_mode:
                ld      a,(fontMode)
                bit     3,a             ; Are we in header mode?
                jp      nz,.find_end    ; Yes, ignore font change
                or      b
.cont_apply     ld      (fontMode),a
                or      $80
                ldi     (de),a
                jp      .find_end

.undo           ld      a,(hl)
                and     a               ; End of line?
                ret     z
                cp      'i'
                jr      z,.no_italic
                cp      'b'
                jr      z,.no_bold

                ; Unknown undo command - ignore
                jp      .find_end

.no_bold:
                ld      b,~2
                jr      .unapply_mode

.no_italic:
                ld      b,~1
                
.unapply_mode   ld      a,(fontMode)
                and     b
                jr      .cont_apply

;;----------------------------------------------------------------------------------------------------------------------
;; Actions
;; 
;; Input:
;;      HL = source of argument (in Scratch2) [page aligned]
;;      DE = current destination (in Scratch) [page aligned]
;;      A' = action index (indexes ActionTable)
;;

CurrentAction   db      0

ActionTable:
                db      4,"LINK"
                db      0

ActionHandlers:
                dw      actionLink

actionLink:
                ld      a,kLinkStart
                ld      (de),a
                inc     e
                ld      a,(CurrentAction)
                inc     a               ; Ensure it's not zero (make it 1-based)
                ld      (de),a          ; Insert action index
                inc     e

                ; Insert URL (in upper case)
.l1             ld      a,(hl)
                and     a
                jr      z,.end

                ; Make it uppercase
                cp      'a'
                jr      c,.not_lc
                cp      'z'+1
                jr      nc,.not_lc
                and     ~$20             ; Make uppercase

.not_lc
                inc     l
                ld      (de),a
                inc     e
                jr      .l1

.end:
                ld      a,kLinkEnd
                ld      (de),a
                inc     e
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Meta data collection

;; Input:
;;      BC = maximum size of output buffer
;;      DE = input buffer
;;      HL = output buffer
;;

CollectMetaData:
                ; Skip any initial whitespace
.l1             ldi     a,(de)
                or      a
                jr      z,.done
                cp      ' '+1
                jr      c,.l1
                cp      $80
                jr      nc,.l1
.done:
                push    hl              ; Store buffer to write to
                dec     de              ; DE points to start of meta data
                xor     a
                ld      hl,de
                cpir                    ; Find end of meta-data
                and     a
                sbc     hl,de           ; HL = length
                ld      bc,hl
                pop     hl
                ex      de,hl
                ldir                    ; Copy data
                ret

cmdTitle:
                ld      hl,MetaTitle
                ld      bc,63
                jp      CollectMetaData

cmdAuthor:
                ld      hl,MetaAuthor
                ld      bc,63
                jp      CollectMetaData

cmdVersion:
                ld      hl,MetaVersion
                ld      bc,31
                jp      CollectMetaData

cmdDate:
                ld      hl,MetaDate
                ld      bc,31
                jp      CollectMetaData

cmdBuild:
                ld      hl,MetaBuild
                ld      bc,31
                jp      CollectMetaData

cmdCopyright:
                ld      hl,MetaCopyright
                ld      bc,64
                jp      CollectMetaData

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
