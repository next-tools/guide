;;----------------------------------------------------------------------------------------------------------------------
;; Various utilities required throughout all the source code
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; 16-bit compare

compare16:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       CF, ZF = results of comparison:
        ;
        ;               CF      ZF      Result
        ;               -----------------------------------
        ;               0       0       HL > DE
        ;               0       1       HL == DE
        ;               1       0       HL < DE
        ;               1       1       Impossible
        ;
                push    hl
                and     a
                sbc     hl,de
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; 16-bit negate

negate16:
        ; Input:
        ;       HL = value
        ; Output:
        ;       HL = -value
        ; Destroys:
        ;       AF
        ;
        xor a
        sub l
        ld l,a
        sbc a,a
        sub h
        ld h,a
        ret

;;----------------------------------------------------------------------------------------------------------------------
;; max

max:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       HL = maximum value
        ;       DE = minimum value
        ;       CF = 1 if DE was maximum
        ;
                and     a
                sbc     hl,de
                jr      c,.choose_2nd   ; HL < DE?  Choose DE!
                add     hl,de           ; Restore HL
                ret
.choose_2nd     add     hl,de
                ex      de,hl
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; alignUp8K
;; align a 16-bit value up to the nearest 8K
;;
;; Input:
;;      HL = value
;;
;; Output:
;;      HL = aligned value

alignUp8K:
                push    af
                ld      a,l
                and     a               ; LSB == 0?
                jr      nz,.alignup
                ld      a,h
                and     $1f
                jr      nz,.alignup

                ; Value is already aligned
.end            pop     af
                ret

.alignup:
                ld      l,0
                ld      a,h
                and     $e0
                add     a,$20
                ld      h,a
                jr      .end

;;----------------------------------------------------------------------------------------------------------------------
;; addrToPage
;; Converts a 16-bit offset to a page index and 8K offset
;;
;; Input:
;;      HL = offset
;;
;; Output:
;;      A = page index
;;      HL = offset within page
;;

addrToPage:
        ; HL = PPP OOOOO OOOOOOOO (P = page, O = offset)
        ld      a,h
        push    af
        and     $1f
        ld      h,a
        pop     af
        and     $e0
        swapnib
        srl     a
        ret

;;----------------------------------------------------------------------------------------------------------------------
;; testRange
;; Test that A >= B and A < C
;;
;; Input:
;;      A = test value
;;      B = start of range
;;      C = past end of range
;;
;; Output:
;;      CF = 1 if A is within range
;;

testRange:
                cp      b
                jr      c,.no
                cp      c
                jr      nc,.no
                ret
.no             and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strcmp
;; Test a string until a difference occurs.  A will be the difference (1st different char - 2nd different char).
;; This means that A < 0 then first string comes before second string.  If A == 0 (or Z flag is set), strings are the
;; same up until they are terminated (with a 0).
;;
;; Input:
;;      DE = 1st string (signed characters)
;;      HL = 2nd string (signed characters)
;;
;; Output:
;;      S = difference of first different character (0 if strings are the same)
;;      ZF = 1 if strings are the same
;;

strcmp:
                push    bc
                push    de
                push    hl

.l1             ld      a,(de)
                cpi                     ; ZF affected by comparison
                jr      nz,.no_match    ; If ZF=0, then a difference is discovered
                inc     de

                ; Both characters are the same, so check for null termination for both
                and     a
                jr      nz,.l1

                jr      .end

.no_match       ; A = difference (or 0 if both terminated)
                dec     hl
                sub     (hl)
.end            pop     hl
                pop     de
                pop     bc
                ret

