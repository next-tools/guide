;;----------------------------------------------------------------------------------------------------------------------
;; MD rendering
;;

;;----------------------------------------------------------------------------------------------------------------------
;; Constants


kWriteEnd               equ     0
kWriteSpace             equ     2
kWriteText              equ     4

Stripe:         db      128,0

textNext        db      'N',0,'ext',0
textPrev        db      'P',0,'revious',0
textBack        db      'B',0,'ack',0
textIndex       db      'I',0,'ndex',0
textContents    db      'C',0,'ontents',0
textMain        db      'M',0,'ain',0
textQuit        db      'Q',0,'uit',0
textAbout       db      'A',0,'bout',0
textLink        db      'ENT/Arrows:',0,'Navigate links',0

ViewData:       
                ; Status bar background
                db      kWriteSpace, kStatusColour
                dw      0, $0150

                ; Title
                db      kWriteText, kStatusColour
                dw      1, FileName

                ; Node name
                db      kWriteText, kStatusHiColour
                dw      72-15
.node_name      dw      0

                ; Stripes
                db      kWriteText, %011'0'010
                dw      74, Stripe
                db      kWriteText, %010'0'110
                dw      75, Stripe
                db      kWriteText, %110'0'100
                dw      76, Stripe
                db      kWriteText, %100'0'101
                dw      77, Stripe
                db      kWriteText, %101'0'011
                dw      78, Stripe

                ; Instructions
                db      kWriteText, %000'1'100
                dw      $0100,textNext
                db      kWriteText, %000'0'111
                dw      $0101,textNext+2
                db      kWriteText, %000'1'100
                dw      $0105,textPrev
                db      kWriteText, %000'0'111
                dw      $0106,textPrev+2
                db      kWriteText, %000'1'100
                dw      $010e,textBack
                db      kWriteText, %000'0'111
                dw      $010f,textBack+2
                db      kWriteText, %000'1'100
                dw      $0113,textIndex
                db      kWriteText, %000'0'111
                dw      $0114,textIndex+2
                db      kWriteText, %000'1'100
                dw      $0119,textContents
                db      kWriteText, %000'0'111
                dw      $011a,textContents+2
                db      kWriteText, %000'1'100
                dw      $0122,textMain
                db      kWriteText, %000'0'111
                dw      $0123,textMain+2
                db      kWriteText, %000'1'100
                dw      $0127,textQuit
                db      kWriteText, %000'0'111
                dw      $0128,textQuit+2
                db      kWriteText, %000'1'100
                dw      $012c,textLink
                db      kWriteText, %000'0'111
                dw      $0138,textLink+12

                db      kWriteText, %000'1'100
                dw      $014b,textAbout
                db      kWriteText, %000'0'111
                dw      $014c,textAbout+2

                db      kWriteEnd

WriteTable:     dw      viewWriteEnd
                dw      viewWriteSpace
                dw      viewWriteText

;;----------------------------------------------------------------------------------------------------------------------
;; renderStatus

renderStatus:
                ld      a,(NodesPage)
                nextreg NR_MMU5,a
                ld      a,(NodeIndex)
                call    getNodeInfoAddrX
                ld      a,(ix+6)                ; Get name index
                ld      b,a
                call    getNodeNamesAddr        ; HL = name information
                inc     hl                      ; HL = name
                ld      (ViewData.node_name),hl

                ld      de,ViewData
.nextElem       ld      hl,WriteTable
                ld      a,(de)
                inc     de
                add     hl,a
                ldhl
                callhl
                jr      .nextElem

viewWriteEnd:   pop     af
                ret

viewWriteSpace: ex      de,hl
                ld      a,(hl)          ; A = colour
                inc     hl
                ld      c,(hl)
                inc     hl
                ld      b,(hl)          ; BC = position
                inc     hl
                ld      e,(hl)
                inc     hl
                ld      d,(hl)          ; DE = size
                inc     hl

                ; Adjust B (Y-position for screen size)
                push    af
                ld      a,(ScreenStatusY)
                add     a,b
                ld      b,a
                pop     af

                call    writeSpace
                ex      de,hl
                ret

viewWriteText:  ex      de,hl
                ld      a,(hl)          ; A = colour
                inc     hl
                ld      c,(hl)
                inc     hl
                ld      b,(hl)          ; BC = position
                inc     hl
                ld      e,(hl)
                inc     hl
                ld      d,(hl)          ; DE = text
                inc     hl

                ; Adjust B (Y-position for screen size)
                push    af
                ld      a,(ScreenStatusY)
                add     a,b
                ld      b,a
                pop     af

                push    hl
                call    writeText
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Render node

colourTable:
                db      kNormalColour<<1
                db      kItalicColour<<1
                db      kBoldColour<<1
                db      kBoldItalicColour<<1
                db      kLinkColour<<1
                db      kLinkColour<<1
                db      kLinkColour<<1
                db      kSelectColour<<1

                db      kHeader1Colour<<1
                db      kHeader2Colour<<1
                db      kHeader3Colour<<1
                db      kHeader4Colour<<1
                db      0
                db      0
                db      0
                db      0

currentColour:  db      0

renderNode:
                ld      a,kNormalColour<<1
                ld      (currentColour),a

                ; Get first line
                ld      hl,(Top)
                call    getLineInfo
                ld      e,a
                ld      a,(DocArena)
                call    arenaPrepare16K         ; HL = actual address of line
                ld      a,(ScreenHeight)
                dec     a
                dec     a                       ; Window height
                ld      b,a                     ; B = number of lines to render
                ld      a,e
                ld      de,(ScreenTop)
                ld      c,0                     ; Current character index in line

.l1:
                ; A = current page index
                ; DE = render position
                ; HL = current address of line
                ; B = number of lines left to render

                push    bc      ; Store loop counting
                push    af      ; Store current page index

                ; Render characters in line until we reach $0a (newline) or $00 (end of node), or reach physical end
                ; of screen
.next_char:
                ; Check for end of screen, node or line
                ld      a,c
                cp      80              ; Reached end of screen?
                jr      z,.eos
                ld      a,(hl)
                and     a               ; Reached end of node?
                jr      z,.eol
                inc     hl
                cp      $0a             ; Reached end of line?
                jr      z,.eol

                ; Check for special codes
                cp      $80             ; TODO: move code range to $10-$1f?
                jr      nc,.code

                ldi     (de),a          ; Write character
                ld      a,(currentColour)
                ldi     (de),a          ; Write colour
                inc     c               ; Increase the number of characters rendered on this line
                jr      .next_char

.code:
                cp      $90             ; Check for colour codes
                jr      nc,.next_char

                ; Check for link start, and search for link end
                cp      kLinkStart
                jr      nz,.not_link
                inc     hl              ; Skip link
.l2             ld      a,(hl)
                cp      kLinkEnd
                jr      z,.not_link
                inc     hl
                jr      .l2

                ; Handle colour code
.not_link
                push    hl
                ld      hl,colourTable
                and     ~$80            ; Mask off the colour code bit
                add     hl,a
                ld      a,(hl)          ; A = new colour
                pop     hl
                ld      (currentColour),a
                jr      .next_char

.eol:
                ; Fill in the rest of the line with spaces
                ; HL = address of terminator
                ld      a,32
                ldi     (de),a
                ld      a,kNormalColour<<1
                ldi     (de),a
                inc     c
                ld      a,c
                cp      80
                jr      nz,.eol         ; Keep clearing out to end of line
                jr      .next_line

.eos:
                ; Advance to end of line
                ld      a,(hl)
                and     a
                jr      z,.next_line
                inc     hl
                cp      $0a
                jr      nz,.eos

                ; Adjust page index and offset if necessary
.next_line:
                ld      a,h
                cp      $e0             ; Reached 8K boundary?
                jr      c,.in_page      ; Not yet, keep going.

                sub     $e0
                ld      h,a             ; Convert real address to offset in next page
                pop     af              ; Restore page index
                inc     a               ; Move to next page
                push    de
                ld      e,a
                ld      a,(DocArena)
                call    arenaPrepare16K         ; A = page index, HL = real address
                pop     de
                push    af

.in_page:
                pop     af
                pop     bc              ; Get count back and reset character count
                djnz    .l1

                ; Render scrolling indicators
                ld      bc,(Top)
                ld      a,b
                or      c
                jr      z,.no_scrup
                ld      hl,(ScreenTop)
                ld      a,79*2
                add     hl,a            ; HL = screen position of UP scrolling indicator
                ldi     (hl),129
                ld      (hl),kBoldItalicColour<<1

.no_scrup:
                ld      hl,(Top)
                ld      a,(ScreenHeight)
                dec     a
                add     hl,a            ; HL = Line # past bottom of screen
                ld      de,(NumLines)   ; DE = Number of lines
                call    compare16       ; Line # past screen < number of lines
                jr      nc,.no_scrdn

                ld      hl,(ScreenTop)
                ld      a,(ScreenHeight)
                dec     a
                dec     a
                ld      d,a
                ld      e,160
                mul     de
                add     hl,de
                dec     hl
                dec     hl
                ldi     (hl),130
                ld      (hl),kBoldItalicColour<<1

.no_scrdn:

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; ensureView
;; Ensure that the given line number in HL is visible on the screen.
;;

ensureView:
                ld      de,(Top)        ; DE = top line
                call    compare16       ; Line # < top line
                jr      c,.adjust
                ld      a,(ScreenHeight)
                sub     3
                ex      de,hl
                add     hl,a            ; HL = last visible line, DE = Line #
                call    compare16       ; Last visible line < line #
                ex      de,hl
                ret     nc

.adjust:
                ; Set top to be at most kWindowHeight/2 lines before line #
                ; HL = Line #
                ld      a,(ScreenHeight)
                sub     2
                srl     a
                ld      e,a
                ld      d,0
                call    compare16       ; Line # in first lines in document?
                jr      c,.go_top
                sbc     hl,de           ; HL = new top
                jr      .set_top
.go_top:        ld      hl,0
.set_top:       ld      (Top),hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
