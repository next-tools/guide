;;----------------------------------------------------------------------------------------------------------------------
;; String utilities
;; All strings unless stated otherwise are null terminated
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; hasSuffix
;; Test to see if string has a suffix
;;
;; Input:
;;      HL = string
;;      DE = suffix string
;;
;; Output:
;;      ZF = 0 if string at HL does not have suffix at DE
;;      ZF = 1 if it does
;;
;; Affects:
;;      AF
;;

hasSuffix:
                push    bc,de,hl

                ld      bc,de           ; BC = beginning of suffix
                call    endString       ; HL points to null terminator
                ex      de,hl
                call    endString
                ex      de,hl

.l1:
                ld      a,(de)
                cp      (hl)
                jr      nz,.no_match

                ld      a,b
                cp      d
                jr      nz,.keep_going
                ld      a,c
                cp      e
                jr      nz,.keep_going

                ; We've matched entire suffix
                xor     a               ; ZF=1
.no_match:
                pop     hl,de,bc
                ret

.keep_going:
                dec     hl
                dec     de
                jr      .l1

;;----------------------------------------------------------------------------------------------------------------------
;; hasPrefix
;; Test to see if string has a prefix
;;
;; Input:
;;      HL = string
;;      DE = prefix string
;;
;; Output:
;;      ZF = 0 if string at HL does not have prefix at DE
;;      ZF = 1 if it does
;;
;; Affects:
;;      AF
;;

hasPrefix:
                push    de,hl
.l1:
                ld      a,(de)
                and     a
                jr      z,.end

                cp      (hl)
                jr      nz,.end

                inc     de
                inc     hl
                jr      .l1
.end:
                pop     hl,de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; endString
;; Find the end of a string.
;;
;; Input:
;;      HL = string
;;
;; Output:
;;      HL = address of null terminator at end of string
;;      ZF = 0
;;
;; Affects:
;;      A
;;

endString:
                ld      a,(hl)
                and     a
                ret     z
                inc     hl
                jr      endString

;;----------------------------------------------------------------------------------------------------------------------
;; strlen
;; Find the length of a string
;;
;; Input:
;;      HL = string
;;
;; Output:
;;      BC = length
;;      ZF = 0
;;
;; Affects:
;;      A
;;

strlen:
                push    hl
                ld      bc,0
.l1:            ld      a,(hl)
                and     a
                jr      z,.end
                inc     bc
                inc     hl
                jr      .l1
.end:           pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strcat
;; Append a string on the end of another.  This does not bounds checking.  Overwrites the buffer of the 1st string.
;;
;; Input:
;;      HL = 1st string
;;      DE = 2nd string
;;
;; Output:
;;      DE = end of 2nd string
;;      HL = 1st+2nd string concatenated
;;
;; Affects:
;;      A
;;

strcat:
                push    hl
                call    endString
.l1             ld      a,(de)
                ld      (hl),a
                and     a
                jr      z,.end
                inc     de
                inc     hl
                jr      .l1
.end:           pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strUpper
;; Convert a string to uppercase
;;
;; Input:
;;      HL = string
;;
;; Output:
;;      HL = string
;;      ZF = 1
;;      CF = 0
;;

strUpper:
                push    hl
.l1:
                ld      a,(hl)
                and     a
                jr      z,.end

                cp      'a'
                jr      c,.ok
                cp      'z'+1
                jr      nc,.ok
                sub     32
.ok:
                ldi     (hl),a
                jr      .l1

.end:
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strcpy
;; Copy a string into the buffer of another
;;
;; Input:
;;      HL = buffer to hold copy
;;      DE = string to copy
;;
;; Output:
;;      DE = end of 2nd string
;;
;; Affects:
;;      A
;;

strcpy:
                push    hl
.l1:
                ldi     a,(de)
                ldi     (hl),a
                and     a
                jr      nz,.l1

                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strrchr
;; Find the last instance of a character in a string.
;;
;; Input:
;;      A = character to search for
;;      HL = string to search in
;;
;; Output:
;;      HL = 0 (not found) or address of last instance
;;
;; Affects:
;;      A
;;

strrchr:
                push    bc,de
                ld      c,a
                ld      de,hl
                call    endString

.l1:
                ld      a,(hl)
                cp      c               ; Found character
                jr      z,.end

                call    compare16
                jr      z,.no_match

                dec     hl
                jr      .l1

.no_match:
                ld      hl,0

.end:
                pop     de,bc
                ret
