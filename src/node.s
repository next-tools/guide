;;----------------------------------------------------------------------------------------------------------------------
;; Node management
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; prepareNode
;; Sets up a node for first viewing
;;
;; Input:
;;      A = node index
;;
;; Output:
;;      Node variables set
;;      CF = 1 if error occurred ()
;;

prepareNode:
                ; Make sure the nodes page is paged in
                push    af
                ld      a,(NodesPage)
                nextreg NR_MMU5,a
                pop     af

                ld      (NodeIndex),a
                ld      c,a
                call    getNodeInfoAddr         ; HL = node info
                ldi     a,(hl)                  ; A = page index in doc arena of node
                ld      (CurrentNodePage),a
                ld      de,(hl)                 ; DE = page offset in doc arena of node
                ld      (CurrentNodeOffset),de
                xor     a
                ld      (Top),a
                ld      (Top+1),a

                call    calcNumLinks
                ld      (NumLinks),a
                ld      a,$ff                   ; No link selected
                ld      (CurrentLink),a

                call    processLines
                ld      a,1
                ld      (Redraw),a              ; Redraw everything!
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; processLines
;; Go through a node and process the lines to find the address for each line
;;
;; Uses:
;;      AF, C, DE, HL, IX
;;

processLines:
                ld      a,(CurrentNodePage)
                ld      e,a
                ld      c,a                     ; C = node page
                ld      a,(DocArena)
                ld      hl,(CurrentNodeOffset)         ; A = arena, EHL = address
                call    arenaPrepare16K         ; Make the document visible in $c000-$ffff, HL = address of start

                ld      a,(NodeLinesPage)
                nextreg NR_MMU5,a               ; Page in the node lines we'll write to at $a000-$bfff

                ld      de,kLineOffsetsAddr     ; $a000-$afff = 2048 offsets
                ld      ix,kLinePagesAddr       ; $b000-$b7ff = 2048 page indices
                xor     a
                ld      (NumLines),a
                ld      (NumLines+1),a

.l1:
                ; Increase counter
                push    bc
                ld      bc,(NumLines)
                inc     bc
                ld      (NumLines),bc
                pop     bc
                
                ; Start of line
                ld      a,(hl)
                and     a                       ; Reached end?
                jr      nz,.more_lines

.end:
                cpl                             ; A = $ff
                ld      (ix+0),a                ; Mark end 
                ret

.more_lines:
                ld      a,d
                cp      $b0                     ; Reached end - no more lines
                jr      nz,.enough_lines

                ; Write a NULL into the document to truncate the node
                xor     a
                ld      (hl),a
                ret

.enough_lines:
                ldi     (ix+0),c                ; Write page index of this line
                ld      a,l
                ldi     (de),a
                ld      a,h
                sub     $c0
                ldi     (de),a                 ; Write page offset of this line

                ; Find the end of the line
.l2             ldi     a,(hl)
                and     a
                jr      z,.end
                cp      $0a
                jr      nz,.l2

                ; Update the page index if necessary
                ld      a,h
                cp      $e0
                jr      c,.l1                   ; No update necessary

                inc     c
                sub     $e0                     ; Reset the offset
                ld      h,a

                push    de
                ld      a,(DocArena)
                ld      e,c
                call    arenaPrepare16K
                pop     de
                jr      .l1

;;----------------------------------------------------------------------------------------------------------------------
;; calcNumLinks
;; Calculate the number of links in the current node
;;
;; Ouput:
;;      A = number of links

calcNumLinks:
                push    iy
                ld      a,(NodeLinesPage)
                nextreg NR_MMU5,a               ; Page in node line information into MMU5 ($a000-$bfff)

                ld      a,(CurrentNodePage)
                ld      e,a
                ld      a,(DocArena)
                ld      hl,(CurrentNodeOffset)
                call    arenaPrepare16K         ; Load area of node into $c000-$ffff, with start between $c000-$dfff.

                ld      ix,0                    ; Current line number
                ld      bc,0                    ; B = number of links, C = Current line offset
                ld      iy,kLinkLinesAddr
                ld      de,kLinkOffsetsAddr

.l1             ld      a,d
                cp      (kLinkOffsetsAddr/256)+1
                jr      z,.end                  ; We've read 256 links before reaching end?

                ldi     a,(hl)
                and     a
                jr      z,.end                  ; Reached end of node
                cp      $a                      ; End of line?
                jr      z,.eol
                cp      kLinkCode               ; Found a link
                jr      z,.link

                ; Normal character
                inc     c
                jr      .l1

.eol            ; Reach end of line
                inc     ix
                ld      c,0
                jr      .l1

.link           ; Found link code
                ld      a,ixl
                ld      (iy+0),a
                ld      a,ixh
                ld      (iy+1),a                ; Write link line
                ld      a,c
                ld      (de),a
                inc     iy        
                inc     iy
                inc     de
                inc     c
                inc     b

                ; Search for the end of the link (assume link is fully formed - starts with kLinkCode and ends with
                ; kLinkEnd)
.l2             ldi     a,(hl)
                inc     c
                cp      kLinkEnd
                jr      nz,.l2
                jr      .l1

.end            ; Wrap up the data
                ld      a,b
                pop     iy
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; getLineInfo
;; Get the page index and offset for the line numbered in BC.  A is $ff if reached past last line.
;;
;; Input:
;;      HL = line number
;;
;; Output:
;;      E = page index (or $ff if last line+1)
;;      HL = page offset
;;
;; Uses:
;;      A, D
;;

getLineInfo:
                ld      a,(NodeLinesPage)
                nextreg NR_MMU5,a
                ex      de,hl                   ; DE = line number
                ld      hl,kLinePagesAddr
                add     hl,de
                ld      a,(hl)                  ; A = page index
                ld      hl,kLineOffsetsAddr
                add     hl,de
                add     hl,de
                ldi     e,(hl)
                ld      h,(hl)
                ld      l,e                     ; HL = page offset
                ld      e,a                     ; E = page index
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Page in a line into 16K area with line starting in first page.
;;
;; Input:
;;      HL = line #
;;
;; Output:
;;      HL = real address with $c000-$ffff
;;
;; Uses:
;;      A, DE
;;

pageLine:
                call    getLineInfo
                ld      a,(DocArena)
                call    arenaPrepare16K         ; HL = address of line
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; scrollDown

scrollDown:
                ld      hl,(Top)
                inc     hl
                ld      bc,hl
                call    getLineInfo
                ld      a,e
                cp      $ff
                ret     z               ; No more lines to scroll

                ld      (Top),bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; scrollUp

scrollUp:
                ld      bc,(Top)
                ld      a,b
                or      c
                ret     z               ; Already at the top of the document, can't scroll any more

                dec     bc
                ld      (Top),bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; gotoLine

gotoLine:
                ld      (Top),bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDown

pageDown:
                ld      b,30
.l1             push    bc
                call    scrollDown
                pop     bc
                ret     z
                djnz    .l1
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDown

pageUp:
                ld      b,30
.l1             push    bc
                call    scrollUp
                pop     bc
                ret     z
                djnz    .l1
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
;; L I N K   N A V I G A T I O N
;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Return information on the given link
;;
;; Input:
;;      A = link #
;;
;; Output:
;;      HL = link line
;;      E = link offset
;;
;; Uses:
;;      A
;;

getLinkInfo:
                ld      e,a
                ld      a,(NodeLinesPage)
                nextreg NR_MMU5,a               ; Page in the meta data
                ld      a,e
                ld      hl,kLinkOffsetsAddr
                add     hl,a
                ld      e,(hl)                  ; E = link offset
                ld      hl,kLinkLinesAddr
                add     hl,a
                add     hl,a
                ldi     a,(hl)
                ld      h,(hl)
                ld      l,a                     ; HL = line #
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Get link data and page in line
;;
;; Input:
;;      A = link #
;;
;; Output:
;;      HL = start of link data, terminated by kLinkEnd
;;

getLinkData:
                call    getLinkInfo             ; HL = line, E = link offset
                push    de
                call    pageLine                ; HL = real address
                pop     de
                ld      a,e
                add     hl,a                    ; HL = start of link

.l1             ldi     a,(hl)
                cp      kLinkStart
                ret     z                       ; HL will point at beginning of data
                jr      .l1

;;----------------------------------------------------------------------------------------------------------------------
;; Return out of calling function if there is no link selected, and also have A = current link index

checkValidLink:
                ld      a,(CurrentLink)
                cp      $ff
                ret     nz                      ; Valid link index so carry on!
                pop     hl                      ; Not a valid link index so drop the return address
                ret                             ; And continue to calling function.

;;----------------------------------------------------------------------------------------------------------------------
;; Go to the next link and wrap if reached end

nextLink:
                ld      a,(NumLinks)
                and     a
                ret     z                       ; Return if we don't have any links on this page

                call    unselectLink
                inc     a
                ld      hl,NumLinks
                cp      (hl)                    ; new index < numLinks
                jr      nz,.end                 ; Yes, continue
                xor     a                       ; No, reset to 0 index
.end            ld      (CurrentLink),a
                jp      selectLink

;;----------------------------------------------------------------------------------------------------------------------
;; Go to previous link and wrap if reached beginning

prevLink:
                ld      a,(NumLinks)
                and     a
                ret     z                       ; Return if we don't have any links on this page

                call    unselectLink
                cp      $ff
                jr      nz,.valid_link
                ld      a,(NumLinks)
.valid_link:    dec     a
                cp      $ff
                jr      nz,.end
                ld      a,(NumLinks)
                dec     a
.end:           ld      (CurrentLink),a
                jp      selectLink

;;----------------------------------------------------------------------------------------------------------------------
;; setLink
;; Insert code at beginning of current link
;;
;; Input:
;;      C = code
;;
;; Output:
;;      A = link #
;;

setLink:
                ld      a,(CurrentLink)
                call    getLinkInfo             ; HL = line #, E = offset within line
                push    de
                call    pageLine                ; HL = address of link's line
                pop     de
                ld      a,e
                add     hl,a                    ; HL = address of link's start
                ld      (hl),c
                ld      a,(CurrentLink)
                ret                

;;----------------------------------------------------------------------------------------------------------------------
;; Insert a new colour code at the current link so that it looks highlighted when rendering

selectLink:
                call    checkValidLink
                ld      c,kLinkSelect
                call    setLink

                ; Ensure that the view shows link's line
                call    getLinkInfo             ; HL = link line
                jp      ensureView

;;----------------------------------------------------------------------------------------------------------------------
;; Reset the colour code at the current link so that it looks highlighted when rendering

unselectLink:
                call    checkValidLink
                ld      c,kLinkCode
                jp      setLink

;;----------------------------------------------------------------------------------------------------------------------
;; nextNode
;; Go to the next node in the list

nextNode:
                call    unselectLink
                ld      a,(NodesPage)
                nextreg NR_MMU5,a
                ld      a,(NodeIndex)
                ld      c,a
                call    getNodeInfoAddr         ; HL = info
                ld      a,3
                add     hl,a
                ld      a,(hl)                  ; A = next node to show
                cp      $ff
                ret     z
                call    pushBackState
                jp      prepareNode

;;----------------------------------------------------------------------------------------------------------------------
;; nextNode
;; Go to the previous node in the list

prevNode:
                call    unselectLink
                ld      a,(NodesPage)
                nextreg NR_MMU5,a
                ld      a,(NodeIndex)
                ld      c,a
                call    getNodeInfoAddr         ; HL = info
                ld      a,4
                add     hl,a
                ld      a,(hl)                  ; A = next node to show
                cp      $ff
                ret     z
                call    pushBackState
                jp      prepareNode

;;----------------------------------------------------------------------------------------------------------------------
;; contentsNode
;; Go to the contents node associated with the current node

contentsNode:
                call    unselectLink
                ld      a,(NodesPage)
                nextreg NR_MMU5,a
                ld      a,(NodeIndex)
                ld      c,a
                call    getNodeInfoAddr         ; HL = info
                ld      a,5
                add     hl,a
                ld      a,(hl)                  ; A = next node to show
                cp      $ff
                ret     z
                call    pushBackState
                jp      prepareNode

;;----------------------------------------------------------------------------------------------------------------------
;; indexNode
;; Go the the index node

indexNode:
                call    unselectLink
                ld      a,(IndexNode)
                cp      $ff
                ret     z
                call    pushBackState
                jp      prepareNode

;;----------------------------------------------------------------------------------------------------------------------
;; mainNode
;; Go to the first node

mainNode:
                call    unselectLink
                xor     a
                call    pushBackState
                jp      prepareNode

;;----------------------------------------------------------------------------------------------------------------------
;; aboutNode
;; Go to the about node

aboutNode:
                call    unselectLink
                ld      a,(AboutNode)
                call    pushBackState
                jp      prepareNode

;;----------------------------------------------------------------------------------------------------------------------
;; followLink
;; Follow the link

LinkHandlers:
                dw      gotoLink

followLink:
                call    unselectLink
                ld      a,(CurrentLink)
                cp      $ff
                ret     z
                call    getLinkData             ; HL = link data
                ldi     a,(hl)
                ex      de,hl                   ; DE = link data
                dec     a                       ; Make it 0-based again (it was 1-based)
                add     a,a
                ld      hl,LinkHandlers
                add     hl,a
                ldhl                            ; HL = link handler
                jp      (hl)

gotoLink:
                ; For normal embedded links
                ; DE = link data
                ; Terminate data with 0
                ld      hl,de                   ; Store beginning of data in HL
.l1             ldi     a,(de)
                cp      kLinkEnd
                jr      nz,.l1
                dec     de
                xor     a
                ld      (de),a                  ; NULL terminate string
                push    de                      ; Store address to fix later

                ld      a,(NodesPage)
                nextreg NR_MMU5,a               ; Page in the node info page
                call    checkNameHL             ; B = node name index
                pop     de
                ld      a,kLinkEnd
                ld      (de),a                  ; Undo the NULL terminator
                jr      nc,.not_found           ; Nothing more to do if the link is not found

                ; We've found the name
                call    getNodeNamesAddr        ; HL = node name information
                ld      a,(hl)                  ; Get real node index
                cp      $ff                     ; Valid?
                jr      z,.not_found            ; No, nothing more to do

                call    pushBackState
                jp      prepareNode             ; Go to it

.not_found:
                ld      a,(CurrentLink)
                call    selectLink
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; findNode:
;;
;; Input:
;;      HL = node name
;;
;; Output;
;;      A = node #
;;      CF=0 if not found
;;

findNode:
                ld      a,(NodesPage)
                nextreg NR_MMU5,a
                call    checkNameHL             ; B = node name index
                ret     nc                      ; Not found?
                call    getNodeNamesAddr
                ld      a,(hl)                  ; Get node #
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Back Buffers
;; This is implemented with a 1280 byte ($500) buffer in the NodeLinesPage page.

                STRUCT  BackData

Page            BYTE
TopLine         WORD
Link            BYTE

                ENDS

BackInfo        BackData        0, 0, 0

;;----------------------------------------------------------------------------------------------------------------------
;; initBackBuffer
;; Initialise the back buffer to get ready to store back info.

initBackBuffer:
                ld      hl,kBackBufferAddr
                ld      (BackBufferMark),hl
                inc     hl
                ld      (BackBufferWrite),hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; writeBackByte
;; Write a single byte in the back buffer, making room (erasing least recently used entry) if necessary.

writeBackByte:
                ld      c,a                     ; Store byte to write
                ld      hl,(BackBufferWrite)
                ld      de,(BackBufferMark)
                call    compare16
                jr      nz,.enough_room

                ; Advance mark to next back buffer entry
                ld      a,BackData              ; Advanced to next write position (erasing value)
                add     de,a
                ex      de,hl
                call    backBufferWrap
                ex      de,hl
                ld      (BackBufferMark),de
.enough_room:
                ldi     (hl),c
                call    backBufferWrap
                ld      (BackBufferWrite),hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; backBufferWrap
;; Ensure that the pointer HL is within the back buffer

backBufferWrap:
                ld      a,h
                cp      kBackBufferPageLimit
                jr      z,.wrap_start
                cp      (kBackBufferAddr/256)-1
                ret     nz

                ; HL has moved past the start of the buffer
                ld      a,kBackBufferPages
                add     a,h
                ld      h,a
                ret
.wrap_start:
                ld      a,h
                sub     kBackBufferPages
                ld      h,a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; readBackByte
;; Read a single byte from HL within the back buffer, taking care of wrap-around if necessary after advancing HL.

readBackByte:
                ld      a,(hl)
                push    af
                inc     hl
                call    backBufferWrap
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pushBackState
;; Push the current viewing state on the back buffer

pushBackState:
                push    af
                ld      a,(NodeLinesPage)
                nextreg NR_MMU5,a
                ld      a,(NodeIndex)
                call    writeBackByte
                ld      a,(Top)
                call    writeBackByte
                ld      a,(Top+1)
                call    writeBackByte
                ld      a,(CurrentLink)
                call    writeBackByte
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; popBackState
;; Pop the current viewing state of the back buffer

popBackState:
                call    unselectLink
                ld      a,(NodeLinesPage)
                nextreg NR_MMU5,a

                ; Calculate start of top of buffer
                ld      hl,(BackBufferWrite)
                dec     hl
                call    backBufferWrap
                ld      de,(BackBufferMark)
                call    compare16
                ret     z                               ; Nothing to pop!

                ; Adjust pointer to previous entry and manage wrap-around
                ld      de,BackData-1
                or      a
                sbc     hl,de
                call    backBufferWrap
                ld      (BackBufferWrite),hl

                ; Read the data
                call    readBackByte
                ld      e,a                             ; E = page #
                call    readBackByte
                ld      c,a
                call    readBackByte
                ld      b,a                             ; BC = top line #
                call    readBackByte
                ld      d,a                             ; D = link #

                ; Prepare the node
                push    bc,de
                ld      a,e
                call    prepareNode
                pop     de

                ; Select the link
                ld      a,d
                cp      $ff
                jr      z,.no_link
                ld      (CurrentLink),a
                call    selectLink

                ; Jump to the correct line
.no_link:       pop     bc
                call    gotoLine

                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
