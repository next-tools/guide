;;----------------------------------------------------------------------------------------------------------------------
;; Global variables
;;----------------------------------------------------------------------------------------------------------------------


;;----------------------------------------------------------------------------------------------------------------------
;; Arenas

DocArena                db      0       ; Arena containing the whole formatted document

;;----------------------------------------------------------------------------------------------------------------------
;; Pages

NodesPage               db      0       ; Contains meta data for all nodes
NodeLinesPage           db      0       ; Page for storing the node lines and link lines
DisplayCode             db      0       ; Page allocated for display code at $8000

;;----------------------------------------------------------------------------------------------------------------------
;; Global data

IndexNode               db      0       ; The node that represents the index of the guide
AboutNode               db      0       ; The node generated to show meta information
BackBufferMark          dw      0       ; Address of the start/end of the back buffer
BackBufferWrite         dw      0       ; Address of the write point of the back buffer

ScreenTop               dw      0       ; Address for top of the screen after being adjusted for display edges
ScreenHeight            db      0       ; Height of whole screen (in tiles) for screen after being adjusted for display edges
ScreenStatusY           db      0       ; Y position of screen status

NumLines                dw      0       ; Number of lines in current node

;;----------------------------------------------------------------------------------------------------------------------
;; Node rendering global variables

CurrentNodePage         db      0       ; Page index of first byte of node
CurrentNodeOffset       dw      0       ; Offset into page of first byte of node
NodeIndex               db      0       ; Currently viewed node index
Top                     dw      0       ; Line number at top of screen

NumLinks                db      0       ; Number of links in current node
CurrentLink             db      0       ; Index of current link (or $ff if none selected)

;;----------------------------------------------------------------------------------------------------------------------
;; Meta information

MetaTitle               ds      64
MetaAuthor              ds      64
MetaVersion             ds      32
MetaDate                ds      32
MetaBuild               ds      32
MetaCopyright           ds      64

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
