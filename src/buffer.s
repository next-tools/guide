;;----------------------------------------------------------------------------------------------------------------------
;; Buffer management
;;----------------------------------------------------------------------------------------------------------------------

BufferPage      db      0       ; Arena page that holds the original source material
BufferMeta      dw      0       ; Address of buffer meta data

;;----------------------------------------------------------------------------------------------------------------------
;; Errors

BufferErrorTable:
                dw      Error_OutOfMemory
                dw      Error_ReadFile
                dw      Error_OpenFile

Error_OutOfMemory:
                db      "Out of memory error!",13
                db      "This could be caused by loading a file that's too large to process on your Next.",0
Error_ReadFile:
                db      "Error reading file",13
                db      "This can be caused by a file system error or reading a file that's greater",13
                db      "than 64 kilobytes in size.",0
Error_OpenFile:
                db      "Error opening file",13
                db      "The file either doesn't exist or it's not a file that can be read.",0

;;----------------------------------------------------------------------------------------------------------------------
;; Buffer meta data

                STRUCT  Buffer

Size            BYTE            ; Size of buffer in number of 8K chunks
LastSize        WORD            ; Size of buffer in last 8K chunk

                ENDS

;;----------------------------------------------------------------------------------------------------------------------
;; bufferDone
;; Deallocates the memory used by the buffer
;;
;; Input:
;;      None
;;
;; Destroys:
;;      AF

bufferDone:     ld      a,(BufferPage)
                call    arenaDone
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferLoad
;; Loads a buffer into memory
;;
;; Input:
;;      IX = filename
;;
;; Output:
;;      A = arena the buffer is loaded into, or zero for error
;;      IX = buffer meta data (if no error occurred)
;;

bufferLoad:
                push    bc
                push    de
                push    hl
                push    iy
                call    GetSetDrive

                ; Open the file
                ld      b,FA_READ
                call    fOpen
                jp      c,.fail_open
                ld      (.BufferHandle),a

                ; Get information on the file
                ld      ix,.FileInfo
                push    ix
                pop     hl
                rst     $8                      ; Call DOS routine
                db      F_FSTAT
                jp      c,.fail_stat
                ld      hl,(.BufferSizeH)
                ld      a,h
                or      l
                jp      nz,.fail_stat           ; >= 64K?

                ; Initialie buffer memory
                call    arenaNew
                jp      c,.fail_stat            ; Not enough memory
                ld      (BufferPage),a          ; Store arena page
                nextreg $54,a                   ; MMU4 set to arena
                ld      bc,Buffer
                call    arenaAlloc              ; Allocate enough memory for buffer meta data
                jp      c,.fail_nomem
                ld      a,h
                or      $80
                ld      h,a                     ; HL = address of meta data
                ld      (BufferMeta),hl
                push    hl
                pop     iy                      ; IY = meta data

                ; Loop and load in the text file 8K (or less) at a time
                xor     a
                ld      (iy+Buffer.Size),a
                ld      bc,(.BufferSize)
                ld      (.BufferSizeH),bc       ; Reuse space for storing original size
.l1             ld      bc,(.BufferSize)
                ld      a,b
                or      c
                jr      z,.done

                ld      a,b
                and     $e0                     ; >= 8K?
                jr      z,.small
                ld      bc,$2000                ; Load 8K
.small          
                ld      a,(iy+Buffer.Size)
                inc     a                       ; 1 more 8K chunk
                ld      (iy+Buffer.Size),a
                and     a
                ld      hl,(.BufferSize)
                ld      (iy+Buffer.LastSize),hl
                sbc     hl,bc                   ; Subtract the amount we're about to load
                ld      (.BufferSize),hl

                ; BC = amount of data to load
                ld      a,(BufferPage)
                call    arenaAlign              ; Go to the next page (and page/allocate one too)
                or      a
                jp      z,.fail_nomem
                ld      a,(.BufferHandle)
                ld      ix,$c000
                call    fRead                   ; Read 8K into MMU7
                jr      .l1

.done           ; Close the file
                ld      a,(.BufferHandle)
                rst     8
                db      F_CLOSE
                ld      ix,(BufferMeta)
                ld      a,(BufferPage)
                jr      .finish

.fail_nomem     xor     a                       ; Out of memory
                call    error
                jr      .cont1
.fail_stat      ld      a,ERR_FileRead          ; Cannot load file error
                call    error
.cont1          ld      a,(.BufferHandle)
                rst     8
                db      F_CLOSE
                jr      .cont2
.fail_open      ld      a,ERR_FileOpen
                call    error
.cont2          xor     a                       ; 0 = error occurred!

.finish         pop     iy
                pop     hl
                pop     de
                pop     bc
                ret

.FileInfo       ds      7
.BufferSize     dw      0
.BufferSizeH    ds      2
.BufferHandle   db      0

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------



