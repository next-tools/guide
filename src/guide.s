;;----------------------------------------------------------------------------------------------------------------------
;; ZX Spectrum Markdown Editor
;; Copyright (C)2020 Matt Davies, all rights reserved.
;;----------------------------------------------------------------------------------------------------------------------

                DEVICE          ZXSPECTRUMNEXT
                CSPECTMAP       "guide.map"

;;----------------------------------------------------------------------------------------------------------------------
;; ROADMAP:
;;      
;;      1.0     *       Basic AmigaGuide functionality without external links
;;      1.1             External links added
;;      1.2             Images supported
;;      1.3             Tables supported
;;      1.4             Running commands
;;
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Memory Map

;; MMU0
;;
;; MMU1
;;      2000    InitKeys, DoneKeys and initial jump
;;      2100    IM table
;;      2201    Code
;;
;;      3b00    String processing buffers
;;      3e00    Keyboard buffer
;;      3f00    Stack
;;
;; MMU2-3       Video
;;      4000
;;
;; MMU4
;;      8000    Buffer meta-data while parsing
;;      8000    Displaying code while displaying (v1.1 onwards)
;;
;; MMU5
;;      a000    Node info page (while parsing) or node line page (while displaying)
;;
;; MMU6-7
;;      c000    Viewing large data structures
;;     

;;----------------------------------------------------------------------------------------------------------------------

GUIDE_VERSION   macro
                db      "1.0a"
                endm

                include "z80n.s"                ; Z80N-specific values and macros
                include "consts.s"              ; Global constants

                ORG     $2000
                jp      Start
                db      "NextGuide V"
                GUIDE_VERSION
                db      0
                dz      "G"                     ; For Phoebus!

                include "utils.s"               ; Various utilities and data structures
                include "filesys.s"             ; File loading and saving
                include "memory.s"              ; Memory management
                include "arena.s"               ; Memory arenas
                include "dot.s"                ; Argument handling
                include "dispedge.s"            ; Display edge detection

                include "src/keyboard.s"            ; Improved keyboard routine
                include "src/vars.s"            ; Global variables
                include "src/buffer.s"          ; Buffer management
                include "src/format.s"          ; Guide format parser
                include "src/scratch.s"         ; Scratch buffer utilities
                include "src/string.s"          ; String processing

;;----------------------------------------------------------------------------------------------------------------------
;; alignUp8
;; align a 16-bit value up to the nearest 8K
;;
;; Input:
;;      A = value
;;
;; Output:
;;      A = aligned value
;;
;; Uses:
;;      B
;;

alignUp8:
                ld      b,a
                and     7
                ld      a,b
                ret     z               ; Value is already aligned

.alignup:
                ld      a,b
                and     ~7
                add     8
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; error handling

ERR_FileOpen    equ     2
ERR_FileRead    equ     4

ErrorCode       db      0

ErrorTable:
                dw      0
                dw      ErrMsg_FileOpen
                dw      ErrMsg_FileRead

error:
                ld      (ErrorCode),a
                ret

ErrMsg_FileOpen:
                db      $89,"Error opening file",$80,$0a,$0a
                db      "The file either doesn't exist or it's not a file that can be read.",0

ErrMsg_FileRead:
                db      $89,"Error reading file",$80,$0a,$0a
                db      "This can be caused by a file system error or reading a file that's greater",$0a
                db      "than 64 kilobytes in size.",0

;;----------------------------------------------------------------------------------------------------------------------
;; Fatal error handling

FatalError      db      0

FatalTable      dw      FERR_OutofMemory

FERR_OutofMemory:
                db      "ERROR: Out Of Memory",$a0

oom:
                push    af
                ld      a,1
                ld      (FatalError),a
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Entry point

FileName:       ds      256
StartNode:      ds      256
HelpFile:       dz      "NextGuide.gde"
GuideSuffix:    dz      ".GDE"
GuidePrefix:    dz      "C:/DOCS/GUIDES/"
HelpFileLen     equ     $-HelpFile
Redraw:         db      1

LoadFile:
                xor     a
                call    error           ; Clear the error
                call    bufferLoad
                or      a               ; ZF = 1: error
                ret

Start:
                ;;
                ;; Set up
                ;;

                di
                ld      (OldSP),sp
                ld      sp,$4000
                call    dotStart

                ; Wait for the keyboard to fully released
.waitNoKey:
                xor a
                in a,($fe)
                cpl
                and 15
                jr nz,.waitNoKey

                ; Parse the arguments
                ld      de,FileName
                call    getArg
                ld      de,StartNode
                call    getArg

                ; Convert the start node argument to uppercase
                ld      hl,StartNode
                call    strUpper

                ; Store the MMU state
                rreg    NR_MMU2
                ld      (OldMMUs+0),a
                rreg    NR_MMU3
                ld      (OldMMUs+1),a
                rreg    NR_MMU4
                ld      (OldMMUs+2),a
                rreg    NR_MMU5
                ld      (OldMMUs+3),a
                rreg    NR_MMU6
                ld      (OldMMUs+4),a
                rreg    NR_MMU7
                ld      (OldMMUs+5),a

                ; Set the state of the MMU to a know state
                nextreg NR_MMU2,$0a
                nextreg NR_MMU3,$0b

                ; Load in the display code
                call    allocPage
                ld      (DisplayCode),a
                nextreg NR_MMU4,a
                dos     M_GETHANDLE
                ld      hl,$8000
                ld      bc,$2000
                dos     F_READ

                ; Attempt to get the display margins
                call    getMargins      ; D = top, E = bottom

                ; Convert # pixels to # tile rows
                ld      a,d
                call    alignUp8
                srl     a
                srl     a
                srl     a
                ld      d,a
                ld      a,e
                call    alignUp8
                srl     a
                srl     a
                srl     a
                ld      e,a

                ; Calculate screen top
                push    de
                ld      e,160
                mul     de
                ld      hl,$4000
                add     hl,de
                pop     de
                ld      (ScreenTop),hl

                ; Calculate screen height
                ld      a,32
                sub     d
                sub     e
                ld      (ScreenHeight),a

                ; Calculate Y position of status bar
                ld      a,$1e
                sub     e
                ld      (ScreenStatusY),a

                call    allocPage       ; Allocate a page for storing line information
                ld      (NodeLinesPage),a

                ;;
                ;; Loading and parsing a document
                ;;
                
                ; Process the arguments (ensure sysvars is paged in)
                ld      ix,FileName
                ld      a,(ix+0)
                and     a
                jr      nz,.load

                ; No filename so load the Markdown help file
                ld      hl,HelpFile
                ld      de,FileName
                ld      bc,HelpFileLen
                ldir
.load:
                ld      hl,FileName
                call    strUpper
                call    LoadFile
                jr      nz,.format

                ; See if it has the .gde extension and try again.
                ld      hl,FileName
                ld      de,GuideSuffix
                call    hasSuffix
                jr      z,.check_guides

                call    strlen          ; BC = length of string at HL
                push    de,hl
                ld      de,255-4        ; Maximum length of file if we were to add .gde to the end
                ld      hl,bc
                call    compare16       ; Length of string < maximum length
                pop     hl,de
                jr      nc,.check_guides

                call    strcat          ; Append the suffix to the filename and try again
                call    LoadFile
                jr      nz,.format

.check_guides:
                ; See if it is in the c:/guides folder and try again.
                ld      hl,FileName
                ld      de,GuidePrefix
                call    hasPrefix
                jr      z,.format       ; Already has c:/guides prefix - so don't bother looking
                ld      hl,FileName
                ld      a,'/'
                call    strrchr         ; Find the last occurrence of '/'
                push    hl              ; HL = path name (without folders) or 0
                ld      hl,Scratch      ; Set up a buffer to house the new filename
                ld      de,GuidePrefix
                call    strcpy          ; Copy prefix into scratch buffer
                pop     de
                ld      a,d
                or      e
                jr      nz,.has_path
                ld      de,FileName
                dec     de
.has_path:
                inc     de
                call    strcat          ; Append the path name

                ex      de,hl
                ld      hl,FileName
                call    strcpy

                call    LoadFile

.format:
                ld      a,(ErrorCode)
                and     a
                jr      z,.format_normal

                ; Generate an error document
                call    startDoc
                call    generateErrorNode
                xor     a
                ld      (StartNode),a
                jr      .after_generation

.format_normal
                call    formatDoc

.after_generation:
                ; Delete the buffer - we don't need it now
                call    bufferDone

                ; From this point onwards, ensure that display code is paged in to $8000-$9fff
                ld      a,(DisplayCode)
                nextreg NR_MMU4,a
                call    checkError
                jp      c,.error

                ; Do we have a start node
                ld      hl,StartNode
                call    findNode
                jr      c,.found_node
                xor     a
.found_node:

                call    prepareNode     ; Ready the initial node

                call    initBackBuffer

                call    initConsole     ; Initialise the tilemap and fonts
                call    textMode        ; Switch to text mode

                ;;
                ;; Main loop
                ;;

.l1:
                ld      a,(Redraw)
                and     a
                jr      z,.no_draw
                xor     a
                ld      (Redraw),a
                call    renderStatus

.no_draw:
                ; TODO: don't render whole screen everytime.  Choose the lines to render
                call    renderNode

                ; Wait for the raster to reach the end of the screen
.waitFrame
                rreg    NR_RASTER_MSB
                and     1
                jr      z,.waitFrame
                rreg    NR_RASTER_LSB
                and     a
                jr      nz,.waitFrame
                call    updateKeyboard

                call    inKey
                jr      z,.waitFrame

                cp      'q'             ; Quit?
                jr      z,.exit

                push    .l1             ; Return point for handlers

                cp      VK_DOWN
                jp      z,scrollDown
                cp      '6'
                jp      z,scrollDown
                cp      VK_UP
                jp      z,scrollUp
                cp      '7'
                jp      z,scrollUp
                cp      ' '
                jp      z,pageDown
                cp      VK_DELETE
                jp      z,pageUp
                cp      VK_RIGHT
                jp      z,nextLink
                cp      '8'
                jp      z,nextLink
                cp      VK_LEFT
                jp      z,prevLink
                cp      '5'
                jp      z,prevLink
                cp      'n'
                jp      z,nextNode
                cp      'p'
                jp      z,prevNode
                cp      'c'
                jp      z,contentsNode
                cp      VK_ENTER
                jp      z,followLink
                cp      '0'
                jp      z,followLink
                cp      'i'
                jp      z,indexNode
                cp      'm'
                jp      z,mainNode
                cp      'b'
                jp      z,popBackState
                cp      'a'
                jp      z,aboutNode

                ret

                ;;
                ;; Exit
                ;;
.exit:
                call    doneConsole     ; Restore pages used by text mode

.error:
                ; Clear up allocated pages
                ld      a,(DocArena)
                call    arenaDone
                ld      a,(NodeLinesPage)
                call    freePage
                ld      a,(NodesPage)
                call    freePage

                ld      a,(DisplayCode)
                call    freePage

.no_load
                DetectLeaks
                ld      a,(FatalError)
                and     a               ; ZF = 0 (if error), CF = 0
                jr      z,.no_error
                ld      hl,FatalTable
                dec     a
                add     hl,a
                add     hl,a
                ldhl
                xor     a
                scf
.no_error:
                jp      dotEnd

;;----------------------------------------------------------------------------------------------------------------------

                display "   BASE: Final address (Max = $3aff): ",$

;;----------------------------------------------------------------------------------------------------------------------
;; Stack & keyboard buffer

                org     $3b00
Scratch2        ds      256             ; 2nd Scratch buffer to store temporary strings etc.
Scratch         ds      256             ; Scratch buffer to store temporary strings etc.
CurrentLine     ds      256             ; Temporary buffer for processsing lines of text
                ds      2*256

;;----------------------------------------------------------------------------------------------------------------------
;; Binary generation

                SAVEBIN "guidebase", $2000, $2000

                org     $8000

                include "display.s"

                display "DISPLAY: Final address (Max = $9fff): ", $

                SAVEBIN "guidedisplay", $8000, $2000

;;----------------------------------------------------------------------------------------------------------------------
;; Font

                org     $0000
Font:           incbin  "data/font.bin"
FontSize:       equ     $-Font

                SAVEBIN "font", $0000, 2048

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
